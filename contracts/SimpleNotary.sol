pragma solidity >=0.5.5;

/**
 * @author Grzegorz Bytniewski
 * @title A simple notary
 */

contract SimpleNotary {

    event Notarization(address indexed notary, bytes32 indexed key, bytes32 value);

    /**
     * @param key for indexing purposes
     * @param value the actual data beaing notarized
     * @notice there might be more than 1 event with the same key for the same notary
     * it's in power of the notary to avoid such situation to happen
     */
    function notarize(bytes32 key, bytes32 value) public {
        require(
            key != bytes32(0x00),
            "SimpleNotary.Notarize: key cannot be empty"
        );
        require(
            value != bytes32(0x00),
            "SimpleNotary.Notarize: value cannot be empty"
        );

        emit Notarization(msg.sender, key, value);
    }
}
