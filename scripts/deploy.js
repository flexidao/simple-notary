//@flow
require('dotenv').load();

const { getProvider } = require("./getProvider") // PROVIDER
const { ContractFactory, Wallet } = require('ethers');
const fs = require("fs");

const SimpleNotary = require('../build/SimpleNotary');
const provider = getProvider();
provider.ready.then(async () => {
    try {
        const secret = fs.readFileSync(__dirname + "/../.secret", "utf-8");
        const deployer = new Wallet(secret, provider);
        console.log("deployer address: %s", deployer.address);

        const simpleNotaryFactory = new ContractFactory(
            SimpleNotary.abi,
            SimpleNotary.bytecode,
            deployer
        );

        const simpleNotary = await simpleNotaryFactory.deploy();
        await simpleNotary.deployed();

        console.log(
            "  DEPLOYMENT" + "\n" +
            "    - SimpleNotary:" + "\n" +
            "        contract:\t" + simpleNotary.address
        );


    } catch (e){
        throw new Error(e)
        process.exit(1);
    }
}
);
