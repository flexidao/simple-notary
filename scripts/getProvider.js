// @flow strict

const { providers } = require('ethers');

const getProvider = () => {
    try {
        let provider;
        if (process.env.JSONRPC_PROVIDER != undefined){
            let port = (process.env.JSONRPC_PORT == "")
                ? ""
                : ":" + process.env.JSONRPC_PORT;
            provider = new providers.JsonRpcProvider(
                {
                    url: process.env.JSONRPC_PROVIDER + port,
                    allowInsecure: !(!!+process.env.PRODUCTION)
                }
            );
        } else if (process.env.IPC_PROVIDER != undefined){
            provider = new providers.IpcProvider(process.env.IPC_PROVIDER);
        }

        return provider;

    } catch (e) {
        throw(e)
    }
}

module.exports = { getProvider };
