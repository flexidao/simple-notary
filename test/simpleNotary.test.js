import chai from 'chai';
import {
    createMockProvider,
    deployContract,
    contractWithWallet,
    getWallets,
    solidity
} from 'ethereum-waffle';

import { utils, Contract, providers, ethers } from 'ethers';

import SimpleNotary from '../build/SimpleNotary';

chai.use(solidity);
const {expect} = chai;

const stringToBytes32 = (string) =>
    utils.padZeros(utils.arrayify(utils.toUtf8Bytes(string)), 32);

const stringToHash = (string) =>
    utils.keccak256(utils.arrayify(utils.toUtf8Bytes(string)));

describe('SimpleNotary: Example', () => {
    let provider = createMockProvider();
    let [wallet, wallet2] = getWallets(provider);
    let simpleNotary;

    beforeEach(async () => {
        simpleNotary = await deployContract(wallet, SimpleNotary);
    });

    it('notarize emits event', async () => {
        const key = stringToBytes32("This is the key");
        const value = stringToHash("This is the value");
        await expect(simpleNotary.notarize(key, value))
            .to.emit(simpleNotary, 'Notarization')
            .withArgs(wallet.address, utils.hexlify(key), utils.hexlify(value));
    });
    it('reverts for empty key', async () => {
        const key = ethers.constants.HashZero;
        const value = stringToHash("This is the value");
        await expect(simpleNotary.notarize(key, value))
            .to.be.revertedWith("SimpleNotary.Notarize: key cannot be empty")
    });
    it('reverts for empty value', async () => {
        const key = stringToBytes32("This is the key");
        const value = ethers.constants.HashZero;
        await expect(simpleNotary.notarize(key, value))
            .to.be.revertedWith("SimpleNotary.Notarize: value cannot be empty")

    });
});
