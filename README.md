#SimpleNotary

##Data structure:

The "notarization" happens through immutable record on the blockchain in an event. The record SCHOULD be uniquely identified based on the **notary** address and/or the **key**, while **notary = msg.sender**.
```
    event Notarization(address indexed notary, bytes32 indexed key, bytes32 value);
```

Anyone can provide the data to the `SimpleNotary` contract through `notirize` function execution.
```
    function notarize(bytes32 key, bytes32 value) public

```

##Prerequisites:
* Node.js >=10
* yarn

##Installation:
To get started using yarn, type:
```
yarn install
```

##Test:
```
yarn compile && yarn test
```

##Deployment:
* private key should be stored in `.secret` file in the root directory of the project
* rpc node url should be provided in `.env` file, like in `.env-example`

then run:
```
yarn compile && yarn prod:deploy
```
contract is deployed on the blockchain under following addresses:
```
Tobalaba: 0xB4B58623DB2D04B8Cd5884634F0e3DeFbEE5684e
```
